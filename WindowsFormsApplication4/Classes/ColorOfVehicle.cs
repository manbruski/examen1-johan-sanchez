﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication4.Classes
{
    public enum ColorOfVehicle
    {
        Red,
        Green,
        Blue,
        Black,
        White,
        Aero,
        AmaranthPink,
        CanaryYellow,
        Bronze,
        FuchsiaPink
        
    }
}
