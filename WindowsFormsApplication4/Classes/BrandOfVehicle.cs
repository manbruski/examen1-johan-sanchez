﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication4.Classes
{
   public enum BrandOfVehicle
    {
        Toyota,
        Hyundai,
        Honda,
        Mitsubishi,
        MercedesBenz,
        Volkswagen,
        Susuki,
        Peugeot,
        Kia,
        Mazda,
        Chevrolet,
        Scania,
        Mack,
        Volvo

    }
}
