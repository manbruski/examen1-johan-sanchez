﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication4.Classes
{
    abstract class Vehicle
    {
        protected string _registration;
        protected int _model;
        protected BrandOfVehicle _brand;
        protected ColorOfVehicle _color;
        protected int _passengerCapacity;
        protected double _fiscalValue;

        public Vehicle(String registration, int model, BrandOfVehicle brand, ColorOfVehicle color, int passengerCapacity, double fiscalValue)
        {
            this._registration = registration;
            this._model = model;
            this._brand = brand;
            this._color = color;
            this._passengerCapacity = passengerCapacity;
            this._fiscalValue = fiscalValue;
        }

        public abstract double CalculateMandatoryInsuranse();

    }
}
