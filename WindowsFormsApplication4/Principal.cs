﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication4.Classes;
using WindowsFormsApplication4.Models;


namespace WindowsFormsApplication4
{
    public partial class Principal : Form
    {
        Dictionary<string, TypeOfVehicle> dictionary;
        Dictionary<string, ColorOfVehicle> dictionary1;
        Dictionary<string, BrandOfVehicle> dictionary2;
        Dictionary<string, TypeOfAutomobile> dictionary3;
        Dictionary<string, TypeOfService> dictionary4;
        Dictionary<string, TypeOfTruck> dictionary5;


        public Principal()
        {
            InitializeComponent();
            //Se declara y llena un objeto Dictionary
            dictionary = new Dictionary<string, TypeOfVehicle>();
            dictionary.Add("Bus", TypeOfVehicle.bus);
            dictionary.Add("Vehiculo", TypeOfVehicle.car);
            dictionary.Add("Camion", TypeOfVehicle.truck);


            dictionary1 = new Dictionary<string, ColorOfVehicle>();
            dictionary1.Add("Aero", ColorOfVehicle.Aero);
            dictionary1.Add("Rosado amaranto", ColorOfVehicle.AmaranthPink);
            dictionary1.Add("Negro", ColorOfVehicle.Black);
            dictionary1.Add("Azul", ColorOfVehicle.Blue);
            dictionary1.Add("Bronce", ColorOfVehicle.Bronze);
            dictionary1.Add("Amarillo Canario", ColorOfVehicle.CanaryYellow);
            dictionary1.Add("Fucsia", ColorOfVehicle.FuchsiaPink);
            dictionary1.Add("Verde", ColorOfVehicle.Green);
            dictionary1.Add("Rojo", ColorOfVehicle.Red);
            dictionary1.Add("Blanco", ColorOfVehicle.White);

            dictionary2 = new Dictionary<string, BrandOfVehicle>();
            dictionary2.Add("Chevrolet", BrandOfVehicle.Chevrolet);
            dictionary2.Add("Honda", BrandOfVehicle.Honda);
            dictionary2.Add("Hyundai", BrandOfVehicle.Hyundai);
            dictionary2.Add("Kia", BrandOfVehicle.Kia);
            dictionary2.Add("Mack”", BrandOfVehicle.Mack);
            dictionary2.Add("Mazda”", BrandOfVehicle.Mazda);
            dictionary2.Add("Mercedes Benz", BrandOfVehicle.MercedesBenz);
            dictionary2.Add("Mitsubishi", BrandOfVehicle.Mitsubishi);
            dictionary2.Add("Peugeot", BrandOfVehicle.Peugeot);
            dictionary2.Add("Scania", BrandOfVehicle.Scania);
            dictionary2.Add("Suzuki", BrandOfVehicle.Susuki);
            dictionary2.Add("Toyota", BrandOfVehicle.Toyota);
            dictionary2.Add("Volkswagen", BrandOfVehicle.Volkswagen);
            dictionary2.Add("Volvo", BrandOfVehicle.Volvo);


            dictionary3 = new Dictionary<string, TypeOfAutomobile>();
            dictionary3.Add("Coupé", TypeOfAutomobile.Coupe);
            dictionary3.Add("Hatchbak", TypeOfAutomobile.Hatchbak);
            dictionary3.Add("PickUp", TypeOfAutomobile.Pickup);
            dictionary3.Add("Sedán", TypeOfAutomobile.Sedan);
            dictionary3.Add("Deportivo Utilitario”", TypeOfAutomobile.SUV);



            dictionary4 = new Dictionary<string, TypeOfService>();
            dictionary4.Add("Public Transport", TypeOfService.PublicTransport);
            dictionary4.Add("Tourism", TypeOfService.Tourism);
            dictionary4.Add("Transporting Of Students", TypeOfService.TransportingOfStudents);


            dictionary5 = new Dictionary<string, TypeOfTruck>();
            dictionary5.Add("Camión de carga pesada”", TypeOfTruck.BallastTractor);
            dictionary5.Add("Mezcladora de cemento”", TypeOfTruck.ConcreteTransportTruck);
            dictionary5.Add("Camión grúa”", TypeOfTruck.CraneTruck);
            dictionary5.Add("Vagoneta de carga", TypeOfTruck.DumpTruck);




            this.groupBox2.Enabled = false;
            this.groupBox3.Enabled = false;
            this.groupBox4.Enabled = false;
            


            this.comboBoxTipo.DisplayMember = "Key";
            this.comboBoxTipo.ValueMember = "Value";
            this.comboBoxTipo.DataSource = new BindingSource(dictionary, null);
            this.comboBoxMarca.DataSource = new BindingSource(dictionary2, null);
            this.comboBoxTipo.DropDownStyle = ComboBoxStyle.DropDownList;

        }

        private void button1_Click(object sender, EventArgs e)
        {

            Vehicle vehicle;

            if ((comboBoxTipo.SelectedValue != null) && (textBoxPlaca.Text != string.Empty)
               && (textBoxModelo.Text != string.Empty) && (comboBoxMarca.SelectedValue != null) && (comboBoxColor.SelectedValue != null)
                && (textBoxCapacidad.Text != string.Empty) && (textBoxValor.Text != string.Empty)) { 
            
            
            }
            //{
            //    //try
            //    //{
            //    //    La siguiente linea provocará una excepción si el dato no es numérico
            //    //    Double.Parse(textBoxValor.Text);

            //    //    switch ((TypeOfVehicle)comboBoxTipo.SelectedValue)
            //    //    {
            //    //        default:
            //    //        case TypeOfVehicle.bus:
            //    //            La siguiente linea provocará una excepción si el dato no es numérico
            //    //            Double.Parse(textBoxAsientos.Text);
                          

            //    //            Instancia en la variable "employee" un objeto "Operator"
            //    //            vehicle = new Bus(
            //    //                txtIdentification.Text,
            //    //                txtName.Text,
            //    //                dtpDateOfEntry.Value,
            //    //                double.Parse(txtBaseSalary.Text));

            //    //            Se hace castign a "employee" para tener acceso a los métodos propios del objeto "Operator"
            //    //            ((Operator)employee).PiecesProduced = int.Parse(txtPiecesProduced.Text);
            //    //            ((Operator)employee).RemunerationPerPiece = double.Parse(txtRemunerationPerPiece.Text);
            //    //            break;

            //    //        case TypeOfEmployee.Seller:
            //    //            La siguiente linea provocará una excepción si el dato no es numérico
            //    //            Double.Parse(txtAmountOfSales.Text);
            //    //            Double.Parse(txtCommission.Text);

            //    //            Instancia en la variable "employee" un objeto "Seller"
            //    //            employee = new Seller(
            //    //                txtIdentification.Text,
            //    //                txtName.Text,
            //    //                dtpDateOfEntry.Value,
            //    //                double.Parse(txtBaseSalary.Text));

            //    //            Se hace castign a "employee" para tener acceso a los métodos propios del objeto "Seller"
            //    //            ((Seller)employee).AmountOfSales = double.Parse(txtAmountOfSales.Text);
            //    //            ((Seller)employee).PercentageOfCommision = double.Parse(txtCommission.Text);
            //    //            break;

            //    //        case TypeOfEmployee.Administrative:
            //    //            La siguiente linea provocará una excepción si el dato no es numérico
            //    //            Double.Parse(txtOvertime.Text);

            //    //            Instancia en la variable "employee" un objeto "Administrative"
            //    //            employee = new Administrative(
            //    //                txtIdentification.Text,
            //    //                txtName.Text,
            //    //                dtpDateOfEntry.Value,
            //    //                double.Parse(txtBaseSalary.Text));

            //    //            Se hace castign a "employee" para tener acceso a los métodos propios del objeto "Administrative"
            //    //            ((Administrative)employee).Overtime = int.Parse(txtOvertime.Text);
            //    //            break;
            //    //    }

            //    //    Haber declarado el objeto base "employee" en lugar de una clase específica evitó repetir
            //    //    estas linea en cada sección del switch-case
            //    //    txtNetEarnings.Text = employee.CalculateSalary().ToString();
            //    //    txtAntiquity.Text = employee.CalculateAtiquity().ToString();
            //    //}
            //    catch (Exception)
            //    {
            //        MessageBox.Show("Se esperaba un valor numérico", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }
            //}
            //else
            //    MessageBox.Show("Debe llenar todos los datos de entrada", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

        }

        private void comboBoxTypeOfVehicle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxTipo.SelectedValue != null)
            {


                switch ((TypeOfVehicle)comboBoxTipo.SelectedValue)
                {
                    case TypeOfVehicle.bus:
                        this.groupBox3.Enabled = true;
                        this.groupBox4.Enabled = false;
                        this.groupBox2.Enabled = false;
                        break;
                    case TypeOfVehicle.car:
                        this.groupBox3.Enabled = false;
                        this.groupBox4.Enabled = false;
                        this.groupBox2.Enabled = true;
                        break;
                    case TypeOfVehicle.truck:
                        this.groupBox3.Enabled = false;
                        this.groupBox4.Enabled = true;
                        this.groupBox2.Enabled = false;
                        break;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBoxPlaca.Text = string.Empty;
                  textBoxModelo.Text = string.Empty;
     
        }
    }
}
