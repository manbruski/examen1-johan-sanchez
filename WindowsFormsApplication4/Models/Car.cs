﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication4.Classes;

namespace WindowsFormsApplication4.Models
{
    class Car : Vehicle
    {
        //Atributos de la clase 
        private TypeOfAutomobile _typeOfAutomobile;
        private int _numberOfDoors;


        
        //Metodo Constructor 
        public Car(String registration, int model, BrandOfVehicle brand, ColorOfVehicle color, int passengerCapacity, double fiscalValue,TypeOfAutomobile typeOfAutomovile)
            : base (registration, model, brand, color, passengerCapacity, fiscalValue)
        {

        }
        //Metodo de la clase abstracta
        public override double CalculateMandatoryInsuranse()
        {
            double MT = 0;
            double VF = this._fiscalValue;
            int AA = Math.Max((DateTime.Today.Year - this._model), 1);
            double MC = calculateTaxForCar();

            switch (this._typeOfAutomobile)
            {
                case TypeOfAutomobile.SUV:
                    MT = (VF*0.1 / AA) + MC;
                    break;
                case TypeOfAutomobile.Sedan:
                    MT = (VF * 0.05 / AA) + MC;
                    break;
                case TypeOfAutomobile.Hatchbak:
                    MT = (VF * 0.07 / AA) + MC;
                    break;
                case TypeOfAutomobile.Pickup:
                    MT = (VF * 0.03 / AA) + MC;
                    break;
                case TypeOfAutomobile.Coupe:
                    MT = (VF * 0.05 / AA) + MC;
                    break;
                default:
                    break;
            }

            return Math.Round(MT);
        }

        public double calculateTaxForCar()
        {
        //(VF/50) * 1% * Cantidad de puertas
            double VF = this._fiscalValue;
            return (VF / 50) * (0.01) * this._numberOfDoors;
           
        }

        //Gets y sets
        public int NumberOfDoors
        {
            get { return _numberOfDoors; }
            set { _numberOfDoors = value; }
        }

    }
}
