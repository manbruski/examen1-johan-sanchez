﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication4.Classes;

namespace WindowsFormsApplication4.Models
{
    class Truck : Vehicle
    {

        //Atributos de la clase
        private TypeOfTruck _typeOfTruck;
        private int _numberOfAxles;


        //Metoodo constructor
        public Truck(String registration, int model, BrandOfVehicle brand, ColorOfVehicle color, int passengerCapacity, double fiscalValue, TypeOfTruck typeOfTruck) : base 
            (registration,model,brand,color, passengerCapacity, fiscalValue)
        {

        }

        //Metodo de la clase abstracta
        public override double CalculateMandatoryInsuranse()
        {
            double MT = 0;
            double VF = this._fiscalValue;
            int AA = Math.Max((DateTime.Today.Year - this._model), 1);
            double MC = calculateTaxForTruck();

            switch (this._typeOfTruck)
            {
                case TypeOfTruck.BallastTractor:
                    MT = (VF * 0.1 / AA) + MC;
                    break;
                case TypeOfTruck.ConcreteTransportTruck:
                    MT = (VF * 0.05 / AA) + MC;
                    break;
                case TypeOfTruck.CraneTruck:
                    MT = (VF * 0.2 / AA) + MC;
                    break;
                case TypeOfTruck.DumpTruck:
                    MT = (VF * 0.05 / AA) + MC;
                    break;
                case TypeOfTruck.LogCarrier:
                    MT = (VF * 0.02 / AA) + MC;
                    break;
                case TypeOfTruck.RefrigerationTruck:
                    MT = (VF * 0.12 / AA) + MC;
                    break;
                case TypeOfTruck.SemiTrailer:
                    MT = (VF * 0.07 / AA) + MC;
                    break;
                case TypeOfTruck.TankTruck:
                    MT = (VF * 0.1 / AA) + MC;
                    break;
                default:
                    break;
            }
            return Math.Round(MT);
        }

        public double calculateTaxForTruck() 
        {
            double VF = this._fiscalValue;
            return (VF / 50)* 0.01 * this._numberOfAxles;
        }



        //Gets y sets
        public int NumberOfAxles
        {
            get { return _numberOfAxles; }
            set { _numberOfAxles = value; }
        }
    }
}
