﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication4.Classes;

    class Bus:Vehicle
    {

        private TypeOfService _typeOfService;
        private int _seatingCapacity;



        public Bus(String registration, int model, BrandOfVehicle brand, ColorOfVehicle color, int passengerCapacity, double fiscalValue, TypeOfService typeOfService)
            : base(registration, model, brand, color, passengerCapacity, fiscalValue)
        {

        }
        //Metodo de la clase abstracta
        public override double CalculateMandatoryInsuranse()
        {

            double MT = 0;
            double VF = this._fiscalValue;
            int AA = Math.Max((DateTime.Today.Year - this._model), 1);
            double MC = calculateTaxForBus();

            switch (this._typeOfService)
            {
                case TypeOfService.Tourism:
                    MT = ((VF* 0.1) * (AA*0.05)) + MC;
                    break;
                case TypeOfService.PublicTransport:
                    MT = ((VF * 0.05) * (AA * 0.05)) + MC;
                    break;
                case TypeOfService.TransportingOfStudents:
                    MT = ((VF * 0.03) * (AA * 0.05)) + MC;
                    break;
                default:
                    break;
            }
            return Math.Round(MT);
        }

        public double calculateTaxForBus()
        {
          double VF = this._fiscalValue;
          return (VF/150) * (0.01) * this._seatingCapacity;
        }


        //Get y set
        public int SeatingCapacity
        {
            get { return _seatingCapacity; }
            set { _seatingCapacity = value; }
        }

    
}
